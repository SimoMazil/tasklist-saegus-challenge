export default {
  user: {
    fetchOne: async userToken => {
      try {
        const response = await fetch('http://localhost:8080/users/api/fetchOne', 
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${userToken}`
          }
        })
        
        const status = await response.status
        const json = await response.json()

        return {status, json}
      } catch (error) {
        console.log(`Fetch Error: ${error.message}`);
      }
    },
    signin: async data => {
      try {
        const response = await fetch('http://localhost:8080/users/api/signin', 
        {
          method: 'POST',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json'
          } 
        })
        
        const status = await response.status
        const json = await response.json()

        return {status, json}
      } catch (error) {
        console.log(`Fetch Error: ${error.message}`);
      }
    },
    signup: async data => {
      try {
        const response = await fetch('http://localhost:8080/users/api/signup', 
        {
          method: 'POST',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json'
          } 
        })
        const status = await response.status
        const json = await response.json()
        return { status, json }
      } catch (error) {
        console.log(`Fetch Error: ${error.message}`);
      }
    }
  },
  tasklist: {
    fetchAll: async userToken => {
      try {
        const response = await fetch('http://localhost:8080/tasklists/api/fetchAll',
        {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${userToken}`
          }
        })
        const status = await response.status
        const json = await response.json()

        return { status, json }
      } catch (error) {
        console.log(`Fetch Error: ${error.message}`);
      }
    },
    add: async (userToken, name) => {
      try {
        const response = await fetch('http://localhost:8080/tasklists/api/add', 
        {
          method: 'POST',
          body: JSON.stringify({name}),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${userToken}`
          } 
        })
        const status = await response.status
        const json = await response.json()
        return { status, json }
      } catch (error) {
        console.log(`Fetch Error: ${error.message}`);
      }
    },
    delete: async tasklistId => {
      try {
        const response = await fetch('http://localhost:8080/tasklists/api/delete', 
        {
          method: 'DELETE',
          body: JSON.stringify({tasklistId}),
          headers: {
            'Content-Type': 'application/json'
          } 
        })
        const status = await response.status
        const json = await response.json()
        return { status, json };
      } catch (error) {
        console.log(`Fetch Error: ${error.message}`);
      }
    }
  },
  task: {
    fetchAll: async (userToken, tasklistId) => {
      try {
        const response = await fetch('http://localhost:8080/tasks/api/fetchAll',
        {
          method: 'POST',
          body: JSON.stringify({tasklistId}),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${userToken}`
          }
        })
        const status = await response.status
        const json = await response.json()
        return { status, json }
      } catch (error) {
        console.log(`Fetch Error: ${error.message}`);
      }
    },
    add: async (userToken, shortDescription, longDescription, endDate, tasklistId) => {
      try {
        const response = await fetch('http://localhost:8080/tasks/api/add', 
        {
          method: 'POST',
          body: JSON.stringify({shortDescription, longDescription, endDate, tasklistId}),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${userToken}`
          }
        })
        const status = await response.status
        const json = await response.json()
        return { status, json }
      } catch (error) {
        console.log(`Fetch Error: ${error.message}`);
      }
    },
    update: async (taskId, finished) => {
      const doc = { finished }
      try {
        const response = await fetch('http://localhost:8080/tasks/api/update', 
        {
          method: 'PUT',
          body: JSON.stringify({taskId, doc}),
          headers: {
            'Content-Type': 'application/json'
          }
        })
        const res = await response
        return res;
      } catch (error) {
        console.log(`Fetch Error: ${error.message}`);
      }
    },
    delete: async taskId => {
      try {
        const response = await fetch('http://localhost:8080/tasks/api/delete', 
        {
          method: 'DELETE',
          body: JSON.stringify({taskId}),
          headers: {
            'Content-Type': 'application/json'
          } 
        })
        const status = await response.status
        const json = await response.json()
        return { status, json };
      } catch (error) {
        console.log(`Fetch Error: ${error.message}`);
      }
    }
  }
}