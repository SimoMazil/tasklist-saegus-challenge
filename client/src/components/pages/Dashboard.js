import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react'

import Credentials from './Credentials'
import Main from './Main'
import LeftSidebar from './LeftSidebar'
import RightSidebar from './RightSidebar'

import api from '../../api';

class Dashboard extends Component {

  state = {
    selectedTasklistId: null,
    userToken: null,
    tasklists: [],
    tasks: [],
    taskDetail: {}
  }

  UNSAFE_componentWillMount() {
    const userToken = localStorage.userToken
    this.setState({ userToken })
  }

  componentDidMount() {
    this.fetchAllTasklists(this.state.userToken)
  }

  componentWillUnmount() {
    localStorage.userToken = null
  }

  fetchAllTasklists = userToken => api.tasklist.fetchAll(userToken).then(response => {
    if(response) {
      if(response.status === 200) this.setState({
        tasklists: [...this.state.tasklists, ...response.json.tasklists]
      })
      return response;
    }
  })

  addTasklist = tasklistName => api.tasklist.add(this.state.userToken, tasklistName).then(response => {
    if(response) {
      if(response.status === 200) this.setState({
        tasklists: [...this.state.tasklists, response.json.tasklistRecord]
      })
      return response;
    }
  })

  deleteTasklist = tasklistId => api.tasklist.delete(tasklistId).then(response => {
    if(response) {
      if(response.status === 200) this.setState({
        tasklists: this.state.tasklists.filter(item => item._id !== response.json.tasklistRecord._id)
      })
      return response;
    }
  })

  selectTasklist = tasklistId => {
    this.setState({ selectedTasklistId: tasklistId, taskDetail: {} })
    this.fetchAllTasks(tasklistId)
  }

  fetchAllTasks = tasklistId => api.task.fetchAll(this.state.userToken, tasklistId).then(response => {
    if(response) {
      if(response.status === 200) this.setState({
        tasks: response.json.tasks
      })
      return response;
    }
  })

  addTask = (shortDesc, longDesc, endDate, tasklistId) => api.task.add(this.state.userToken, shortDesc, longDesc, endDate, tasklistId)
  .then(response => {
    if(response) {
      if(response.status === 200) this.setState({
        tasks: [...this.state.tasks, response.json.taskRecord]
      })
      return response;
    }
  })

  finishTask = taskId => api.task.update(taskId, true).then(response => {
    if(response) {
      if(response.status === 200) this.setState({
        tasks: this.state.tasks.filter(task => task._id === taskId ? task.finished = true : task)
      })
      return response;
    }
  })

  unFinishTask = taskId => api.task.update(taskId, false).then(response => {
    if(response) {
      if(response.status === 200) this.setState({
        tasks: this.state.tasks.filter(task => {
          if(task._id === taskId) {
            task.finished = false
            return task
          } else {
            return task
          }
        })
      })
      return response;
    }
  })

  taskDetail = taskId => this.setState({
    taskDetail: this.state.tasks.filter(task => task._id === taskId)[0]
  })

  deleteTask = taskId => api.task.delete(taskId).then(response => {
    if(response) {
      if(response.status === 200) this.setState({
        tasks: this.state.tasks.filter(task => task._id !== response.json.taskRecord._id),
        taskDetail: {}
      })
      return response;
    }
  })
  
  render() {
    const { userToken, tasklists, selectedTasklistId, tasks, taskDetail } = this.state
    return (
      <div>
      {
        !userToken 
        ? <Credentials /> : 
        <div style={{padding: '10px'}}>
          <Grid>
            <Grid.Row>
              <Grid.Column width={3}>
                <LeftSidebar tasklists={tasklists} addTasklist={this.addTasklist} deleteTasklist={this.deleteTasklist} selectTasklist={this.selectTasklist}/>
              </Grid.Column>
              <Grid.Column width={10}>
                <Main selectedTasklistId={selectedTasklistId} tasks={tasks} addTask={this.addTask} finishTask={this.finishTask} unFinishTask={this.unFinishTask} taskDetail={this.taskDetail}/>
              </Grid.Column>
              <Grid.Column width={3}>
                <RightSidebar selectedTasklistId={selectedTasklistId} taskDetail={taskDetail} deleteTask={this.deleteTask}/>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      }
    </div>
    );
  }
}
 
export default Dashboard;