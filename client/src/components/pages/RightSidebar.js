import React, { useState } from 'react';
import { Card, Button, Modal } from 'semantic-ui-react';

import { GetFormattedDate } from '../../utils/formatDates'

const RightSidebar = ({selectedTasklistId, taskDetail, deleteTask}) => {

  const [show, setShow] = useState(false)

  const showModal = () => {
    setShow(true)
  }

  const onDelete = (e, {taskid}) => {
    deleteTask(taskid).then(res => res.status === 406 && alert(res.json.errors.message))
    onClose()
  }

  const onClose = () => {
    setShow(false)
  }
  
  return (
    <div>
      {
        selectedTasklistId && Object.keys(taskDetail).length 
        ?
        <div>
          <Card>
            <Card.Content>
              <Card.Header>{taskDetail.shortDescription}</Card.Header>
              {taskDetail.finished && 'Task finished'}
              <Card.Meta>End Date: {GetFormattedDate(taskDetail.endDate)}</Card.Meta>
              <Card.Description>
                {taskDetail.longDescription}
              </Card.Description>
              <Card.Meta>Created Date: {GetFormattedDate(taskDetail.createdAt)}</Card.Meta>
            </Card.Content>
            <Card.Content extra>
              <Button basic color='red' fluid onClick={showModal}>
                Delete Task
              </Button>
            </Card.Content>
          </Card>

          <Modal size='mini' open={show}>
            <Modal.Header>Delete Task</Modal.Header>
            <Modal.Content>
              <p>Are you sure you wanna delete this task!</p>
            </Modal.Content>
            <Modal.Actions>
              <Button negative onClick={onClose}>No</Button>
              <Button positive onClick={onDelete} taskid={taskDetail._id}>Yes, delete</Button>
            </Modal.Actions>
          </Modal>
        </div>
        : ''
      }
    </div>
  )
}

export default RightSidebar