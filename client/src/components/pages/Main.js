import React, { useState } from 'react';
import { Form, Button, Divider } from 'semantic-ui-react'

import Tasks from '../lists/Tasks';
import FinishedTasks from '../lists/FinishedTasks';

const Main = ({selectedTasklistId, tasks, addTask, finishTask, unFinishTask, taskDetail}) => {

  const [shortDesc, setShortDesc] = useState('')
  const [longDesc, setLongDesc] = useState('')
  const [endDate, setEndDate] = useState('')

  const onAddTask = () => {
    addTask(shortDesc, longDesc, endDate, selectedTasklistId)
    .then(res => {
      if(res.status === 406) return alert(res.json.errors.message)
      setShortDesc('')
      setLongDesc('')
      setEndDate('')
    })
  }

  return (
    <div>
      {
        !selectedTasklistId ? 'No tasklist has been selected!' : 
        <div>
          <Tasks tasks={tasks} finishTask={finishTask} taskDetail={taskDetail}/>
          
          <Divider />
          
          <FinishedTasks tasks={tasks} unFinishTask={unFinishTask} taskDetail={taskDetail}/>

          <Divider />
          
          <Form>
            <Form.Input type='text' name='shortDescription' label='Shoft Description' placeholder='Shoft description' value={shortDesc} onChange={e => setShortDesc(e.target.value)}/>
            <Form.TextArea label='Long Description' placeholder='Long description' value={longDesc} onChange={e => setLongDesc(e.target.value)}/>
            <Form.Input type='date' name='endDate' label='End Date' placeholder='End date' value={endDate} onChange={e => setEndDate(e.target.value)}/>
            <Button type='submit' color='yellow' fluid onClick={onAddTask}>Add task</Button>
          </Form>
        </div>
      }
    </div>
  )
}

export default Main