import React, { useState } from 'react';
import { Menu, Icon, Header, Button, Form, Modal } from 'semantic-ui-react';

const LeftSidebar = ({tasklists, addTasklist, deleteTasklist, selectTasklist}) => {

  const [name, setName] = useState('')
  const [show, setShow] = useState(false)
  const [tasklistId, setTasklistId] = useState('')

  const onAdd = () => {
    addTasklist(name).then(res => res.status === 406 && alert(res.json.errors.message))
    setName('')
  }

  const onSelect = (e, {tasklistid}) => selectTasklist(tasklistid)

  const showModal = (e, {tasklistid}) => {
    setShow(true)
    setTasklistId(tasklistid)
  }

  const onDelete = () => {
    deleteTasklist(tasklistId).then(res => res.status === 406 && alert(res.json.errors.message))
    selectTasklist('')
    setTasklistId('')
    onClose()
  }

  const onClose = () => {
    setShow(false)
    setTasklistId('')
  }

  return (
    <div>
      <Menu vertical fluid>
        <Menu.Item>
          <Header as='h4'>Task lists</Header>
        </Menu.Item>
        {
          tasklists.map(tasklist => {
            return (
              <Menu.Item key={tasklist._id} name={tasklist.name} tasklistid={tasklist._id} onClick={onSelect}>
                {tasklist.name}
                <Icon name='delete' color='red' tasklistid={tasklist._id} onClick={showModal}/>
              </Menu.Item>
            )
          })
        }
      </Menu>

      <Form>
        <Form.Input type='text' placeholder='Task list name' value={name} onChange={e => setName(e.target.value)}/>
        <Button type='submit' fluid color='olive' onClick={onAdd}>Add a task list</Button>
      </Form>

      <Modal size='mini' open={show}>
        <Modal.Header>Delete Tasklist</Modal.Header>
        <Modal.Content>
          <p>All tasks related to this tasklist gonna be delete!</p>
        </Modal.Content>
        <Modal.Actions>
          <Button negative onClick={onClose}>No</Button>
          <Button positive onClick={onDelete}>Yes, delete</Button>
        </Modal.Actions>
      </Modal>
    </div>
  )
}

export default LeftSidebar;