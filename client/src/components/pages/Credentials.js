import React, { Component } from 'react';
import { Divider, Header } from 'semantic-ui-react';

import SignupForm from '../forms/SignupForm';
import SigninForm from '../forms/SigninForm';

import Dashboard from './Dashboard';

import api from '../../api';

class Credentials extends Component {

  state = {
    userToken: null
  }

  UNSAFE_componentWillMount() {
    const userToken = localStorage.userToken
    this.setState({ userToken })
  }
  
  signinSubmit = data => api.user.signin(data).then(response => {
    if(response) {
      if(response.status === 401) return response;
      if(response.status === 200) {
        localStorage.userToken = response.json.user.token
        this.props.history.push('/dashboard')
      }
    } else {
      return {status: 404}
    }
  })

  signupSubmit = data => api.user.signup(data).then(response => {
    if(response) {
      if(response.status === 401) return response;
      if(response.status === 200) {
        localStorage.userToken = response.json.user.token
        this.props.history.push('/dashboard')
      }
    } else {
      return {status: 404}
    }
  })

  render() { 
    return (
      <div>
        {
          this.state.userToken 
          ? <Dashboard /> : 
          <div style={{width: '400px', margin: '50px auto'}}>
            <Header as='h3'>Sign-up Form</Header>
            <SignupForm signupSubmit={this.signupSubmit}/>
            
            <Divider />
            
            <Header as='h3'>Sign-in Form</Header>
            <SigninForm signinSubmit={this.signinSubmit} />
          </div>
        }
      </div>
    );
  }
}
 
export default Credentials;