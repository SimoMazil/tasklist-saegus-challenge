import React, { Component } from 'react';
import { Form, Button } from 'semantic-ui-react';
import Validator from 'validator';
import PropTypes from 'prop-types';

import ErrorMessage from '../messages/ErrorMessage';

class SigninForm extends Component {
  state = {
    data: {
      email: '',
      password: ''
    },
    loading: false,
    errors: {}
  }

  onChange = e => this.setState({
    data: { ...this.state.data, [e.target.name]: e.target.value}
  })

  onSubmit = () => {
    const data = this.state.data
    const error = this.validator(data)
    if(Object.keys(error).length) return this.setState({ errors: error })
    
    this.setState({ errors: {}, loading: true})
    this.props.signinSubmit(data).then(res => {
      if (res && res.status === 401) this.setState({
        errors: {message: res.json.errors.message},
        loading: false
      })
      if(res && res.status === 404) this.setState({
        errors: {message: "Server Not Found !"},
        loading: false
      })
    })
  }

  validator = data => {
    if(!Validator.isEmail(data.email)) return {field: "Invalide Email"}
    if(!data.password) return {field: "Password can't be blank"}
    return {}
  }
  
  render() {
    const { loading, errors } = this.state

    return (
      <Form onSubmit={this.onSubmit} loading={loading}>
        {
          Object.keys(errors).length > 0 && <ErrorMessage error={errors.field || errors.message} />
        }
        <Form.Input type='text' name='email' label='Email' placeholder='Email' onChange={this.onChange}/>
        <Form.Input type='password' name='password' label='Password' placeholder='Password' onChange={this.onChange}/>
        <Button type='submit' color='olive' fluid>Sign-in</Button>
      </Form>
    );
  }
}

SigninForm.propTypes = {
  signinSubmit: PropTypes.func.isRequired
}
 
export default SigninForm;