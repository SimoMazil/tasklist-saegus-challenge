import React, { Component } from 'react';
import { Form, Button } from 'semantic-ui-react';
import Validator from 'validator';
import PropTypes from 'prop-types';

import ErrorMessage from '../messages/ErrorMessage';

class SignupForm extends Component {
  state = {
    data: {
      firstname: '',
      lastname: '',
      email: '',
      confirmeEmail: '',
      password: '',
      confirmePassword: ''
    },
    loading: false,
    errors: {}
  }

  onChange = e => this.setState({
    data: { ...this.state.data, [e.target.name]: e.target.value }
  })

  onSubmit = () => {
    const data = this.state.data
    const error = this.validator(data)
    if(Object.keys(error).length) return this.setState({ errors: error })
    
    this.setState({ errors: {}, loading: true})
    this.props.signupSubmit(data).then(res => {
      if (res && res.status === 401) this.setState({
        errors: {message: res.json.errors.message},
        loading: false
      })
      if(res && res.status === 404) this.setState({
        errors: {message: "Server Not Found !"},
        loading: false
      })
    })
  }

  validator = data => {
    if(!data.firstname) return {field: "First name can't be blank"}
    if(!data.lastname) return {field: "Last name can't be blank"}
    if(!Validator.isEmail(data.email)) return {field: "Invalide Email"}
    if(!Validator.equals(data.email, data.confirmeEmail)) return {field: "Invalide Confirmation Email"}
    if(!data.password) return {field: "Password can't be blank"}
    if(!Validator.equals(data.password, data.confirmePassword)) return {field: "Invalide Confirmation Password"}
    return {}
  }
  
  render() {
    const { loading, errors } = this.state

    return (
      <Form onSubmit={this.onSubmit} loading={loading}>
        {
          Object.keys(errors).length > 0 && <ErrorMessage error={errors.field || errors.message} />
        }
        <Form.Group widths={2}>
          <Form.Input type='text' name='firstname' label='First name' placeholder='First name' onChange={this.onChange}/>
          <Form.Input type='text' name='lastname' label='Last name' placeholder='Last name' onChange={this.onChange}/>
        </Form.Group>
        <Form.Group widths={2}>
          <Form.Input type='text' name='email' label='Email' placeholder='Email' onChange={this.onChange}/>
          <Form.Input type='text' name='confirmeEmail' label='Confirme email' placeholder='Confirme email' onChange={this.onChange}/>
        </Form.Group>
        <Form.Group widths={2}>
          <Form.Input type='password' name='password' label='Password' placeholder='Password' onChange={this.onChange}/>
          <Form.Input type='password' name='confirmePassword' label='Confirme password' placeholder='Confirme password' onChange={this.onChange}/>
        </Form.Group>
        <Button type='submit' color='yellow' fluid>Sign-up</Button>
      </Form>
    );
  }
}

SignupForm.propTypes = {
  signupSubmit: PropTypes.func.isRequired
}
 
export default SignupForm;