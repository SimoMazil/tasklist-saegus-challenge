import React from 'react';
import { Message } from 'semantic-ui-react';

const ErrorMessage = ({error}) => (
  <Message negative>
    <Message.Header>{error}</Message.Header>
  </Message>
)

export default ErrorMessage