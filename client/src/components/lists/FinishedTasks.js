import React, { useState } from 'react';
import { Button, List, Header, Segment } from 'semantic-ui-react'

import { GetFormattedDate } from '../../utils/formatDates'

const Finished = ({tasks, unFinishTask, taskDetail}) => {
  
  const [showFinished, setShowFinished] = useState(false)

  const onUnFinish = (e, {taskid}) => unFinishTask(taskid)

  const onDetail = (e, {taskid}) => taskDetail(taskid)

  return (
    <div>
      <Header as='h2'>
        <Button color='teal' floated='right' size='mini' onClick={() => setShowFinished(!showFinished)}>Show list</Button>
        List of finished tasks
      </Header>
      <br />
      {
        showFinished && <Segment inverted>
          <List divided verticalAlign='middle'>
            {
              tasks && tasks.filter(task => task.finished).map(task => {
                return (
                  <List.Item key={task._id}>
                  <List.Content floated='right'>
                    <Button icon='checkmark' size='mini' color='olive' taskid={task._id} onClick={onUnFinish}/>
                    <Button icon='arrow right' size='mini' taskid={task._id} onClick={onDetail}/>
                  </List.Content>
                  <List.Content>{task.shortDescription}</List.Content>
                  <small>{GetFormattedDate(task.endDate)}</small>
                </List.Item>
                ) 
              })
            }
          </List>
        </Segment>
      }
    </div>
  )
}

export default Finished