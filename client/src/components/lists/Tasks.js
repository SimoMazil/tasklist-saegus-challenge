import React from 'react';
import { Button, List, Header, Segment } from 'semantic-ui-react'

import { GetFormattedDate } from '../../utils/formatDates'

const Tasks = ({tasks, finishTask, taskDetail}) => {

  const onFinish = (e, {taskid}) => finishTask(taskid)

  const onDetail = (e, {taskid}) => taskDetail(taskid)

  return (
    <div>
      <Header as='h2'>List of tasks</Header>
      <div style={{padding: '20px 0px'}}>
        <Segment inverted>
          <List divided verticalAlign='middle'>
            {
              tasks && tasks.filter(task => !task.finished).map(task => {
                return (
                  <List.Item key={task._id}>
                  <List.Content floated='right'>
                    <Button icon='checkmark' size='mini' taskid={task._id} onClick={onFinish}/>
                    <Button icon='arrow right' size='mini' taskid={task._id} onClick={onDetail}/>
                  </List.Content>
                  <List.Content>{task.shortDescription}</List.Content>
                  <small>{GetFormattedDate(task.endDate)}</small>
                </List.Item>
                ) 
              })
            }
          </List>
        </Segment>
      </div>
    </div>
  )
}

export default Tasks