export const GetFormattedDate = (taskDate) => {
  const date = taskDate && taskDate.slice(0, 10).split('-')
  return date && `${date[2]}/${date[1]}/${date[0]}`
}