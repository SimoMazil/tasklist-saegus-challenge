import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Credentials from './components/pages/Credentials';
import Dashboard from './components/pages/Dashboard';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" component={Credentials} exact />
        <Route path="/dashboard" component={Dashboard} exact />
      </Switch>
    </Router>
  );
}

export default App;
