# tasklist-saegus-challenge

This is a task list app which allow the user to create and manage his tasks in a simple way.

## The back-end

The backend of the this app is built with :

* **node JS**
* **express JS**
* **mongodb**
* **And some other awesome node packages**

The backend is running on the 8080 port.


## The front-end

The frontend of the this app is built with :

* **React JS**

## Usage


You can use docker by running the command : 

```
docker-compose up
```

Now the server running on the port 8080 and the client is running on the port 3000

```
http://localhost:3000
```

🎉 Congrats you start using the app