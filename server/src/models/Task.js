import mongoose from 'mongoose'

const schema = new mongoose.Schema({
  shortDescription: {type: String, required: true},
  longDescription: {type: String},
  endDate: {type: Date, required: true},
  finished: {type: Boolean, default: false},
  userId: {type: String, required: true},
  tasklistId: {type: String, required: true}
}, {timestamps: true})

export default mongoose.model('Task', schema)