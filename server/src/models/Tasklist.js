import mongoose from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'

const schema = new mongoose.Schema({
  name: {type: String, required: true, unique: true},
  userId: {type: String, required: true}
}, {timestamps: true})

schema.plugin(uniqueValidator, {message: "This name already exist!"})

export default mongoose.model('Tasklist', schema)