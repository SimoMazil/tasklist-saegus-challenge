import express from 'express'
import mongoose from 'mongoose'
import dotenv from 'dotenv'
import swaggerUi from 'swagger-ui-express'
import swaggerDoc from './utils/swaggerDoc.js'

import users from './routes/users'
import tasklists from './routes/tasklists'
import tasks from './routes/tasks'

dotenv.config()

const app = express()
app.use(express.json())

mongoose.connect(process.env.MONGO_URL, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false })
.then(() => console.log("DB Connected!"))
.catch(err => console.log(`DB Connection Error: ${err.message}`))

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDoc))

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "http://localhost:3000");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
  next();
}); 

app.use("/users", users)
app.use("/tasklists", tasklists)
app.use("/tasks", tasks)

app.get("/", (req, res) => res.status(200).send("Hello World!"))

app.listen(8080, () => console.log("Running on 8080"))