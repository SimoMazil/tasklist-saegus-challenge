import express from 'express'
import User from '../models/User'

import authenticate from '../middlewares/authenticate';

const router = express.Router()

router.post("/api/signup", (req, res) => {
  const { firstname, lastname, email, password } = req.body
  const user = new User({ firstname, lastname, email })
  user.setPassword(password)
  user.save()
  .then(userRecord => {
    res.status(200).json({user: userRecord.toAuthJSON()})
  })
  .catch(errors => {
    res.status(401).json({errors: {message: "Email already exists !"}})
  })
})

router.post("/api/signin", (req, res) => {
  const { email, password } = req.body
  User.findOne({ email: email }).then(user => {
    if(user && user.isValidPassword(password)) {
      res.status(200).json({user: user.toAuthJSON()})
    } else {
      res.status(401).json({errors: {message: "Invalide Credentials !"}})
    }
  })
})

router.post("/api/fetchOne", authenticate, (req, res) => {
  User.findOne({ _id: req.currentUser._id }).then(user => {
    if(user) {
      res.status(200).json({user: user.toAuthJSON()})
    } else {
      res.status(401).json({errors: {message: "Invalide Credentials !"}})
    }
  })
})

export default router