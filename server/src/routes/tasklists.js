import express from 'express'
import authenticate from '../middlewares/authenticate';
import Tasklist from '../models/Tasklist'
import Task from '../models/Task'

const router = express.Router()

router.get('/api/fetchAll', authenticate, (req, res) => {
  Tasklist.find({ userId: req.currentUser._id })
  .then(tasklists => res.json({ tasklists }))
  .catch(errors => res.status(406).json({ errors }))
})

router.post('/api/add', authenticate, (req, res) => {
  const tasklist = new Tasklist({ ...req.body, userId: req.currentUser._id })
  tasklist.save()
  .then(tasklistRecord => res.json({ tasklistRecord }))
  .catch(errors => res.status(406).json({ errors }))
})

router.delete('/api/delete', (req, res) => {
  const { tasklistId } = req.body
  Tasklist.findOneAndRemove({_id: tasklistId})
  .then(tasklistRecord => {
    Task.deleteMany({tasklistId})
    .then(res => console.log("Tasks deleted!"))
    .catch(error => console.log(`Deleting tasks: ${error}`))
    return res.json({ tasklistRecord })
  })
  .catch(errors => res.status(406).json({ errors }))
})

export default router