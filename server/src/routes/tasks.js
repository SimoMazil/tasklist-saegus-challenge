import express from 'express'
import authenticate from '../middlewares/authenticate';
import Task from '../models/Task'

const router = express.Router()

router.post('/api/fetchAll', authenticate, (req, res) => {
  Task.find({ userId: req.currentUser._id, tasklistId: req.body.tasklistId })
  .then(tasks => res.json({ tasks }))
  .catch(errors => res.status(406).json({ errors }))
})

router.post('/api/add', authenticate, (req, res) => {
  const tasklist = new Task({ ...req.body, userId: req.currentUser._id })
  tasklist.save()
  .then(taskRecord => res.json({ taskRecord }))
  .catch(errors => res.status(406).json({ errors }))
})

router.delete('/api/delete', (req, res) => {
  Task.findOneAndRemove({_id: req.body.taskId})
  .then(taskRecord => res.json({ taskRecord }))
  .catch(errors => res.status(406).json({ errors }))
})

router.put('/api/update', (req, res) => {
  Task.updateOne({_id: req.body.taskId}, req.body.doc)
  .then(taskRecord => res.json({ taskRecord }))
  .catch(errors => res.status(406).json({ errors }))
})

export default router