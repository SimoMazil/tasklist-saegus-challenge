export default {
  "swagger": "2.0",
  "info": {
      "version": "1.0.0",
      "title": "Tasklist back-end API",
      "description": "Tasklist is a small back-end API"
  },
  host: "http://localhost:8080",
  "paths": {
    "/": {
      "get": {
        "summary": "Hello World!",
        "responses": {
          "200": {
            "description": "OK"
          }
        }
      }
    },
    "/users/api/signup": {
        "parameters": [
          {
            "name": "firstname",
            "required": true,
            "description": "First Name of the user",
            "type": "string"
          },
          {
            "name": "lastname",
            "required": true,
            "description": "Last Name of the user",
            "type": "string"
          },
          {
            "name": "email",
            "required": true,
            "description": "Email of the user",
            "type": "string"
          },
          {
            "name": "password",
            "required": true,
            "description": "Password of the user",
            "type": "string"
          },
      ],
      "post": {
        "summary": "Use to create a user",
        "responses": {
          "200": {
            "description": "Return user's credentials"
          },
          "406": {
            "description": "Return an error"
          }
        }
      }
    },
    "/users/api/signin": {
      "parameters": [
          {
            "name": "login",
            "required": true,
            "description": "Email of the user",
            "type": "string"
          },
          {
            "name": "password",
            "required": true,
            "description": "Password of the user",
            "type": "string"
          },
      ],
      "post": {
        "summary": "Use to Sign-in a user",
        "responses": {
          "200": {
            "description": "User is logged in"
          },
          "406": {
            "description": "Show a message: Invalide Credentials"
          }
        }
      }
    },
    "/tasklists/api/fetchAll": {
      "parameters": [
          {
            "name": "token",
            "required": true,
            "description": "A token is passed in the header of the http request",
            "type": "string"
          }
      ],
      "get": {
        "summary": "Use to fetch all the task lists",
        "responses": {
          "200": {
            "description": "Return all task lists",
          },
          "406": {
            "description": "Return an error"
          }
        }
      }
    },
    "/tasklists/api/add": {
      "parameters": [
          {
            "name": "name",
            "required": true,
            "description": "Name of the task list",
            "type": "string"
          },
          {
            "name": "token",
            "required": true,
            "description": "A token is passed in the header of the http request",
            "type": "string"
          }
      ],
      "post": {
        "summary": "Use to create a new task list",
        "responses": {
          "200": {
            "description": "Return the task list created"
          },
          "406": {
            "description": "Return an error"
          }
        }
      }
    },
    "/tasklists/api/delete": {
      "parameters": [
          {
            "name": "tasklistId",
            "required": true,
            "description": "Id of the current task list",
            "type": "string"
          },
      ],
      "delete": {
        "summary": "Use to delete a task list",
        "responses": {
          "200": {
            "description": "Delete the task list and the tasks related to it"
          },
          "406": {
            "description": "Return an error"
          }
        }
      }
    },
    "/tasks/api/fetchAll": {
      "parameters": [
          {
            "name": "token",
            "required": true,
            "description": "A token is passed in the header of the http request",
            "type": "string"
          },
          {
            "name": "tasklistId",
            "required": true,
            "description": "Id of the task list",
            "type": "string"
          }
      ],
      "get": {
        "summary": "Use to fetch all the tasks of a task list",
        "responses": {
          "200": {
            "description": "Return all tasks"
          },
          "406": {
            "description": "Return an error"
          }
        }
      }
    },
    "/tasks/api/add": {
      "parameters": [
          {
            "name": "shortDescription",
            "required": true,
            "description": "Short Description of the task",
            "type": "string"
          },
          {
            "name": "longDescription",
            "required": true,
            "description": "Long Description of the task",
            "type": "string"
          },
          {
            "name": "endDate",
            "required": true,
            "description": "End Date of the task",
            "type": "string"
          },
          {
            "name": "tasklistId",
            "required": true,
            "description": "Id of the task list",
            "type": "string"
          },
          {
            "name": "token",
            "required": true,
            "description": "A token is passed in the header of the http request",
            "type": "string"
          }
      ],
      "post": {
        "summary": "Use to create a new task",
        "responses": {
          "200": {
            "description": "Return the task created"
          },
          "406": {
            "description": "Return an error"
          }
        }
      }
    },
    "/tasks/api/delete": {
      "parameters": [
          {
            "name": "taskId",
            "required": true,
            "description": "Id of the current task",
            "type": "string"
          },
      ],
      "delete": {
        "summary": "Use to delete a task",
        "responses": {
          "200": {
            "description": "Delete the task"
          },
          "406": {
            "description": "Return an error"
          }
        }
      }
    }
  }
}